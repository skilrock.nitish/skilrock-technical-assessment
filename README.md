# Skilrock Technical Assessment - Flutter

Hello! We are excited that you are taking this technical assessment and look forward to reviewing your solutions to each of the challenges.

Please follow the instructions in this readme carefully, we ask that you work on the solution by yourself and submit only your own code for each of the tasks.

Good luck and have fun!

## Table of contents

- [1. Getting started](#1-getting-started)
- [2. Technical tasks](#2-technical-tasks)
- [3. Submission](#3-submission)

---

## 1. Getting started

### 1.1 Create a new private git repo

Login to your Gitlab account, if you don't have one just sign up for a new one, and create a new **private** repo named `skilrock-technical-assessment`.

_❗️**Important**: Please keep this repo private._

## 2. Technical tasks

We expect you to have two branches `master` and `develop`. For each task we ask that create a branch from your `master` with the task name. E.g:

```
$ git branch <feature/task-1-task-name>
```

Please try to spend no more than 5 hours in total.

_❗️**Important**: Raise one PR for each task. Include a brief summary, so we can understand your approach and thinking. If you are not able to complete a task to production quality, we will be expecting the PR description to highlight what is missing, and what should have been added or changed with more time. You are allowed to merge your feature branches to develop but please refrain from merging any branches to master._

---

_❗️**Important**: This assignment is to be done in Flutter, write native Android/iOS code only if required!_

### Task 2.1 - Login/Sign-Up form

As a user, I want to be able to login or sign up.

**Acceptance Criteria**

On the initial landing page:

- Show a form to login/signup.
- Register/Validate the user using auth service. Can use any 3rd party authentication service(Firebase/Auth0/Okta/OpenID etc)
- Form validations and smooth transitions.
- All signups and logins should happen via OTP verification.

### Task 2.2 - Mobile Recharge

As a user, after logging in I want to recharge any mobile number.

**Acceptance Criteria**

- Show a form to get the mobile number, dropdown of available network providers, amount.
- Success page to be shown after the transaction, with all details entered in the form.
- All data should be persistent, you can use SQLite or any other storage service as per your ease or any other state management library.


### Task 2.3 - Showing Transaction History

As a user, I want to view the report of my transactions.

**Acceptance Criteria**

- Show a list of all the recharges done by the user with proper details, i.e, datetime, amount, mobile number, operator.

### Important Points:

- Any user who registers will have 1000 INR as balance initially, on every transaction the balance should be deducted and in case of no balance proper error should be there.
- The user can see the balance available in their account at any time.
- All forms should have proper validations with error messages.

### UI Designs

Design snapshots are in the assets folder of this repo! These are just for reference, and you are not limited to adhere to this strictly. Any icon or media file that you can't find and is required in the UI you can use from the internet! Have a look at the designs, this will give you a more clear idea about the tasks!

---

💡 Bonus task

### Task 2.4 - Accessibility

As a disabled customer, I want the app to be accessible.

**Acceptance Criteria**

- The acceptance criteria is open to your interpretation for this task.
- Based on your knowledge (and limited time) think about what you'd consider a priority for accessibility and implement some improvements.

_💡**Tip**: Do want you can within the time frame, add additional tasks you'd like to have completed to your PR as a comment._

---

## 3. Submission

When you have completed your tasks, build and send us the APK and please add the below mentioned users as contributors to your private repo:

- `skilrock.nitish`

Now let us know you have finished the assessment. We will communicate the next steps to you.

If you have any feedback or questions on the assessment we'd love to hear your thoughts! You can mention the same as an issue in this repo.

Thank you,

From the Skilrock team.
